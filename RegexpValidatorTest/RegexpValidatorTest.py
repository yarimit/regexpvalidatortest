# -*- coding: utf-8 -*-
'''
汎用正規表現パターンバリデータ

CSVに記述されたパラメータから pyunitテストユニットを生成し、実行・結果レポート出力を行う

Created on 2013/09/06

@author: arimitsu
'''
from optparse import OptionParser
import unittest
import xmlrunner
import csv
import re

class RegexpValidator(object):
    pass

    @classmethod
    def generateTestMethod(cls, testParams):
        def validate(self):
            print "test %s (%s)" % (testParams['name'], testParams['note'])
            
            '''
            出現回数チェック
            '''
            f = open(testParams['logfile'])
            logdata = f.read()
            f.close()
            
            
            r = re.compile(testParams['pattern'], re.M)
            finds = r.findall(logdata)
            findCnt = len(finds)
            
            print "find pattern %s, %d times" % (testParams['pattern'], findCnt)
            
            '''
            countのパース
            '''
            count = testParams['count']
            if count.find('-') > -1:
                if count.startswith('-'):
                    cond = int(count[1:])
                    self.assertTrue(findCnt < cond, "Unmatch count %s" % count)
                elif count.endswith('-'):
                    cond = int(count[:-1])
                    self.assertTrue(findCnt > cond, "Unmatch count %s" % count)
                else:
                    conds = count.split('-')
                    self.assertTrue(findCnt >= int(conds[0]), "Unmatch count %s" % count)
                    self.assertTrue(findCnt <= int(conds[1]), "Unmatch count %s" % count)
            else:
                self.assertEquals(findCnt, int(count), "Unmatch count %s" % count)
                
            print "test %s OK" % (testParams['name'])
        return validate

    @classmethod
    def generateTestMethods(cls, testClass, csvFile):
        csvReader = csv.DictReader(open(csvFile, "r"))
        for data in csvReader:
            if data['name']:
                setattr(testClass, 'test' + data['name'], cls.generateTestMethod(data))

    @classmethod
    def generateClass(cls, className="RegexpValidatorTest", csvFile=None):
        test_class = type(className, (unittest.TestCase,), {})
        
        cls.generateTestMethods(test_class, csvFile)
        
        return test_class

def suite(className, csvFile):
    """
    テストスイートを生成
    """
    suite = unittest.TestSuite()
    test_class = RegexpValidator.generateClass(className, csvFile)
    suite.addTest(unittest.makeSuite(test_class))
    return suite

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-c", "--class", dest="className",
                      help="TestClass Name", default="RegexpValidatorTest")
    parser.add_option("-f", "--file", dest="csvFile",
                      help="CSV File", default="./validate.csv")
    (options, args) = parser.parse_args()

#    unittest.TextTestRunner().run(suite())
#    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
    xmlrunner.XMLTestRunner(output='test-reports').run(suite(options.className, options.csvFile))
    
